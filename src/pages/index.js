import React from "react";

import Link from "../components/link.js";
import Layout from "../components/layout";
import Image from "../components/image";
import SEO from "../components/seo";

const IndexPage = () => (
  <Layout>
    <SEO title="Etusivu" />
  </Layout>
);

export default IndexPage;
